package absen.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import absen.models.Biodata;
import absen.repositories.BiodataRepo;

@Controller
@RequestMapping("/biodata")
public class BiodataController {
	@Autowired BiodataRepo biorep;
	
	@GetMapping("index")
	public ModelAndView view() {
		ModelAndView view = new ModelAndView("biodata/index");
		List<Biodata> listbio = this.biorep.findAll();
		view.addObject("listbio", listbio);
		return view;
	}

}
