package absen.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import absen.models.absen;
import absen.repositories.Pegawai1Repo;

@Controller
@RequestMapping(value="/pegawai/")
public class Pegawai1_controller {
	@Autowired Pegawai1Repo perep;
	
	@GetMapping(value="pegawai1")
	public ModelAndView view() {
		ModelAndView view = new ModelAndView("/pegawai/pegawai1");
		List<absen> listpeg = this.perep.findAll();
		view.addObject("listpeg", listpeg);
		return view;
 	}
	
	@GetMapping("pegawai2")
	public ModelAndView view1 () {
		ModelAndView view1 = new ModelAndView("/pegawai/pegawai2");
		List<absen> listpeg1 = this.perep.findAll();
		view1.addObject("listpeg1", listpeg1);
		return view1;
	}

	@GetMapping("admin")
	public ModelAndView view2() {
		ModelAndView view2 = new ModelAndView("/pegawai/admin");
		List<absen>listadmin=this.perep.findAll();
		view2.addObject("listadmin", listadmin);
		return view2;
	}
}
