package absen.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import absen.models.absen;
import absen.repositories.Pegawai1Repo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiControllerPegawai1 {
@Autowired Pegawai1Repo perep;

@GetMapping("/search/{search}")
public ResponseEntity<?> searchdata (@PathVariable ("search") String search){
	try {
		List<absen> search1= this.perep.findAll();
		List<absen> search2 = new ArrayList<absen>();
		search2 = (List<absen>) search1.stream().filter(a -> {
			return a.getNama().equalsIgnoreCase(search);
		}).collect(Collectors.toList());
		return new ResponseEntity<>(search2,HttpStatus.OK);
	}catch (Exception e) {
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}

@GetMapping("/admin")
public ResponseEntity<List<absen>> getalladmin(){
	try {
		List<absen>pegawai1 = this.perep.findAll();
		return new ResponseEntity<List<absen>>(pegawai1,HttpStatus.OK);
	}
	catch(Exception e) {
		return new ResponseEntity<List<absen>>(HttpStatus.NO_CONTENT);
	}
}


	@GetMapping("/pegawai1")
	public ResponseEntity<List<absen>> getall(){
		try {
			List<absen>pegawai1 = this.perep.findAll();
			List<absen>pegawai1f = new ArrayList<absen>();
			pegawai1f = (List<absen>) pegawai1.stream().filter(nama ->{
				return nama.getNama().equals("Muhammad Atsilah Al Mughni Nasution");
			}).collect(Collectors.toList());
			//return new ResponseEntity<List<absen>>(pegawai1,HttpStatus.OK);
			return new ResponseEntity<List<absen>>(pegawai1f,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<List<absen>>(HttpStatus.NO_CONTENT);
		}
	}
	@GetMapping("/pegawai2")
	public ResponseEntity<List<absen>> getpegawai2(){
		try {
			List<absen>pegawai2 = this.perep.findAll();
			List<absen>pegawai2f = new ArrayList<absen>();
			pegawai2f = (List<absen>) pegawai2.stream().filter(nama ->{
				return nama.getNama().equals("Bagus Aryo Nugroho");
			}).collect(Collectors.toList());
			//return new ResponseEntity<List<absen>>(pegawai1,HttpStatus.OK);
			return new ResponseEntity<List<absen>>(pegawai2f,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<List<absen>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/insertabsen")
	public ResponseEntity<absen>insertabsen(@RequestBody absen pegawai){
		try {
			this.perep.save(pegawai);
			return new ResponseEntity<absen>(pegawai,HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<absen>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/pegawai1/{id}")
	public ResponseEntity<absen>getbyid(@PathVariable Long id){
		try {
			absen pegawai = this.perep.findById(id).orElse(null);
			return new ResponseEntity<absen>(pegawai,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<absen>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping("/deleteadmin/{id}")
	public ResponseEntity<absen>deleteabsen(@PathVariable Long id){
		try {
			this.perep.deleteById(id);
			return new ResponseEntity<absen>(HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<absen>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/editadmin/{id}")
	public ResponseEntity<absen>editbyid(@RequestBody absen absen, @PathVariable Long id){
	try {
	absen.setId(id);
	this.perep.save(absen);
	return new ResponseEntity<absen>(absen,HttpStatus.OK);
	}
	catch(Exception e) {
		e.printStackTrace();
		return new ResponseEntity<absen>(HttpStatus.NO_CONTENT);
	}
	}
	
	
}
