package absen.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import absen.models.Biodata;
import absen.repositories.BiodataRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiBiodataController {
@Autowired BiodataRepo biorep;

@GetMapping("/biodata")
public ResponseEntity<List<Biodata>>getall(){
	try {
		List<Biodata>bio=this.biorep.getbio();
		List<Biodata>bio1= new ArrayList<Biodata>();
		bio1=bio.stream().sorted((temp,temp1)->temp.getNama().compareTo(temp1.getNama())).collect(Collectors.toList());
		return new ResponseEntity<List<Biodata>>(bio1,HttpStatus.OK);
		//return new ResponseEntity<List<Biodata>>(bio,HttpStatus.OK);
	}
	catch(Exception e) {
		return new ResponseEntity<List<Biodata>>(HttpStatus.NO_CONTENT);
	}
}
	
@GetMapping("/biodata/{id}")
public ResponseEntity<Biodata>getbyid(@PathVariable Long id){
try {
	Biodata bio = this.biorep.findById(id).orElse(null);
	return new ResponseEntity<Biodata>(bio,HttpStatus.OK);
}
catch(Exception e) {
	return new ResponseEntity<Biodata>(HttpStatus.NO_CONTENT);
}
}
@PostMapping("/insertbio")
public ResponseEntity<Biodata> insertbio(@RequestBody Biodata bio){
	try {
		this.biorep.save(bio);
		return new ResponseEntity<Biodata>(bio,HttpStatus.OK);
	}
	catch(Exception e) {
		return new ResponseEntity<Biodata>(HttpStatus.NO_CONTENT);
	}
}

@PutMapping("/editbio/{id}")
public ResponseEntity<Biodata> edit (@RequestBody Biodata bio, @PathVariable Long id){
	try {
		bio.setId(id);
		this.biorep.save(bio);
		return new ResponseEntity<Biodata>(bio,HttpStatus.OK);
	}
	catch(Exception e) {
		return new ResponseEntity<Biodata>(HttpStatus.NO_CONTENT);
	}
}

@GetMapping("/getbiobyid")
public ResponseEntity<List<Biodata>>getbiobyid(){
	try {
		List<Biodata>bio = this.biorep.getbiobyid(1L);
		return new ResponseEntity<List<Biodata>>(bio,HttpStatus.OK);
	} catch (Exception e) {
		return new ResponseEntity<List<Biodata>>(HttpStatus.NO_CONTENT);
		// TODO: handle exception
	}
}
@GetMapping("/getbiobyid2")
public ResponseEntity<List<Biodata>>getbiobyid2(){
	try {
		List<Biodata>bio = this.biorep.getbiobyid(2L);
		return new ResponseEntity<List<Biodata>>(bio,HttpStatus.OK);
	} catch (Exception e) {
		return new ResponseEntity<List<Biodata>>(HttpStatus.NO_CONTENT);
		// TODO: handle exception
	}
}
}

