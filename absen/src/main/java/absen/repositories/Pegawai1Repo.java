package absen.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import absen.models.absen;

@Repository
public interface Pegawai1Repo extends JpaRepository<absen, Long> {
	@Query(value="select * from biodata b where biodata_id=?1 ",nativeQuery = true)
	List<absen>getbiobyid(Long id);

}
