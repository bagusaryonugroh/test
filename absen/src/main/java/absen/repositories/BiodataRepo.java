package absen.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import absen.models.Biodata;

public interface BiodataRepo extends JpaRepository<Biodata, Long>{
	@Query(value="from Biodata b where b.Is_delete=false")
	List<Biodata> getbio();
	
	@Query(value="select * from Biodata b where b.id=?1",nativeQuery = true)
	List<Biodata>getbiobyid(Long id);

}
