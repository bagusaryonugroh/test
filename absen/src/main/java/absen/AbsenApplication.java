package absen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbsenApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbsenApplication.class, args);
	}

}
