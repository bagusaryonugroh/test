package absen.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.springframework.lang.Nullable;

@Entity
@Table(name="biodata")
public class Biodata {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	
	
	@Nullable
	@Column(name="nama", length =100)
	private String nama;
	
	@Nullable
	@Column(name="NIP")
	private Long nip;
		
	@Nullable
	@Column(name="email", length=50)
	private String email;
	
	@Nullable
	@Column(name="dob")
	private String dob;
	
	@Nullable
	@Column(name="alamat", length=100)
	private String alamat;
	
	@Nullable
	@Column(name="no_telp")
	private Long no_telp;
	
	@NotNull
	@Column(name = "is_delete", columnDefinition = "boolean default false")
	private boolean Is_delete;


	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public boolean isIs_delete() {
		return Is_delete;
	}

	public void setIs_delete(boolean is_delete) {
		Is_delete = is_delete;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Long getNip() {
		return nip;
	}

	public void setNip(Long nip) {
		this.nip = nip;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Long getNo_telp() {
		return no_telp;
	}

	public void setNo_telp(Long no_telp) {
		this.no_telp = no_telp;
	}
	

}
