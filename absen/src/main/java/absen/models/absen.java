package absen.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.lang.Nullable;



@Entity
@Table(name="absen")
public class absen {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@Nullable
	@Column(name="nama", length =100)
	private String nama;
	
	@Nullable
	@Column(name="NIP")
	private Long nip;
	
	@ManyToOne
	@JoinColumn(name="biodata_id",insertable=false, updatable=false)
	public Biodata biodata;
		
	public Biodata getBiodata() {
		return biodata;
	}

	public void setBiodata(Biodata biodata) {
		this.biodata = biodata;
	}

	public Long getBiodata_id() {
		return biodata_id;
	}

	public void setBiodata_id(Long biodata_id) {
		this.biodata_id = biodata_id;
	}

	@Nullable
	@Column(name="biodata_id")
	private Long biodata_id;
	
	@Nullable
	@Column(name="jenis_absen", length=50)
	private String jenis_absen;
	
	public String getTanggal() {
		return tanggal;
	}

	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}

	@Nullable
	@Column(name="tanggal", length=100)
	private String tanggal;
	
	@Nullable
	@Column(name="waktu")
	private String waktu;
	
	@Nullable
	@Column(name="lembur")
	private Long lembur;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Long getNip() {
		return nip;
	}

	public void setNip(Long nip) {
		this.nip = nip;
	}

	public String getJenis_absen() {
		return jenis_absen;
	}

	public void setJenis_absen(String jenis_absen) {
		this.jenis_absen = jenis_absen;
	}


	public String getWaktu() {
		return waktu;
	}

	public void setWaktu(String waktu) {
		this.waktu = waktu;
	}

	public Long getLembur() {
		return lembur;
	}

	public void setLembur(Long lembur) {
		this.lembur = lembur;
	}
	
	

}
